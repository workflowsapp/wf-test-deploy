##### [![Build Status](http://buildserver.workflowsapp.com:8080/buildStatus/icon?job=teams---development)](http://buildserver.workflowsapp.com:8080/job/teams---development/) development — [Logs](https://addons-sso.heroku.com/apps/ebb64920-9b30-4f5c-97c8-e8a12bf3fac8/addons/1ee64741-0eb7-4f34-a9bb-447e60d5a275)

##### [![Build Status](http://buildserver.workflowsapp.com:8080/buildStatus/icon?job=teams---review)](http://buildserver.workflowsapp.com:8080/job/teams---review/) review — [Logs](https://addons-sso.heroku.com/apps/383f9398-a7bc-472c-a807-9844ec6f6168/addons/f74b1b5e-c451-44a1-93d0-b0a423a7567e)

##### [![Build Status](http://buildserver.workflowsapp.com:8080/buildStatus/icon?job=teams---production)](http://buildserver.workflowsapp.com:8080/job/teams---production/) production — [Logs](https://addons-sso.heroku.com/apps/bff81e8a-0874-445c-ad22-9b28c96318be/addons/32757089-97a2-4ee1-95de-32455afcbce9)

***

# Swagger generated server

## Overview
This server was generated by the [swagger-codegen](https://github.com/swagger-api/swagger-codegen) project.  By using the [OpenAPI-Spec](https://github.com/OAI/OpenAPI-Specification) from a remote server, you can easily generate a server stub.  This is an example of building a node.js server.

This example uses the [expressjs](http://expressjs.com/) framework.  To see how to make this your own, look here:

[README](https://github.com/swagger-api/swagger-codegen/blob/master/README.md)

### Running the server
To run the server, run:

```
npm start
```

To view the Swagger UI interface:

```
open http://localhost:8080/docs
```

This project leverages the mega-awesome [swagger-tools](https://github.com/apigee-127/swagger-tools) middleware which does most all the work.