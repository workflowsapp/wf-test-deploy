/**
 * Created by Kokulan on 9/30/2016.
 */

"use strict";
var express = require('express');
var app = express();
var debug = require("debug")("express:server");
var mongoose = require('mongoose')
var http = require("http");
var bodyParser = require('body-parser');


app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
var serverPort = process.env.PORT || 3002;

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', 'POST,GET,OPTIONS,PUT,DELETE');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, authorization, access_token, organization_token");
    next();
})
    .options('*', function (req, res, next) {
        res.end();
    });
var router = express.Router();
app.use('/', router);


router.route("")
    .get(function (req, res) {
        res.send({
            success: true,
            data: {
                env: process.env.NODE_ENV
            }
        });
    })

http.createServer(app).listen(serverPort, function () {
    console.log('Your server is listening ondsadsa port %d (http://localhost:%d)', serverPort, serverPort);
});